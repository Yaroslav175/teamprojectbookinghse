﻿using ClassLibraryBooking.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryBooking
{
    class Context : DbContext
    {
        public DbSet<HSEBuilding> Buildings { get; set; }
        public DbSet<Floor> Floors { get; set; }
        public DbSet<Room> Rooms { get; set; }
        public DbSet<User> Users { get; set; }

        public Context() : base("localsql")
        {
        }
    }
}
