namespace ClassLibraryBooking.Migrations
{
    using Model;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ClassLibraryBooking.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(ClassLibraryBooking.Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            var generalData = Repository.DeserializeGeneralData(isCalledFromSeed: true);

            var allFloors = new List<Floor>();
            foreach (var building in generalData.Buildings)
            {
                allFloors.AddRange(building.Floors);
            }

            var allRooms = new List<Room>();
            foreach (var floor in allFloors)
            {
                allRooms.AddRange(floor.Rooms);
            }

            foreach (var building in generalData.Buildings)
            {
                context.Buildings.AddRange(generalData.Buildings);
            }
            context.SaveChanges();

            foreach (var floor in allFloors)
            {
                context.Floors.AddRange(allFloors);
            }
            context.SaveChanges();

            foreach (var room in allRooms)
            {
                context.Rooms.AddRange(allRooms);
            }
            context.SaveChanges();


            var buildings = context.Buildings.ToList();
            foreach (var building in buildings)
            {
                building.Floors = new List<Floor>();
                var currentBuilding = generalData.Buildings.FirstOrDefault(b => b.Address == building.Address);
                for (int i = 0; i < currentBuilding.Floors.Count; i++)
                {
                    var currentFloor = currentBuilding.Floors[i];
                    building.Floors.Add(context.Floors.FirstOrDefault(f => f.Id == currentFloor.Id && f.Number == currentFloor.Number));
                }
            }
            context.SaveChanges();

            var floors = context.Floors.ToList();
            foreach (var floor in floors)
            {
                floor.Rooms = new List<Room>();
                var currentFloor = generalData.Floors.FirstOrDefault(f => f.Id == floor.Id);
                for (int i = 0; i < currentFloor.Rooms.Count; i++)
                {
                    var currentRoom = currentFloor.Rooms[i];
                    floor.Rooms.Add(context.Rooms.FirstOrDefault(r => r.Id == currentFloor.Id && r.Number == currentRoom.Number && r.Floor == currentRoom.Floor));
                }
            }
            context.SaveChanges();

        }
    }
}
