namespace ClassLibraryBooking.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HSEBuildings",
                c => new
                    {
                        ID = c.Int(nullable: false, identity: true),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.ID);
            
            CreateTable(
                "dbo.Floors",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.String(),
                        HSEBuilding_ID = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.HSEBuildings", t => t.HSEBuilding_ID)
                .Index(t => t.HSEBuilding_ID);
            
            CreateTable(
                "dbo.Rooms",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Number = c.Int(nullable: false),
                        Floor = c.Int(nullable: false),
                        Capacity = c.Int(nullable: false),
                        Computers = c.Boolean(nullable: false),
                        Projector = c.Boolean(nullable: false),
                        LoudSpeakers = c.Boolean(nullable: false),
                        Microphone = c.Boolean(nullable: false),
                        Availability = c.Boolean(nullable: false),
                        Floor_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Floors", t => t.Floor_Id)
                .Index(t => t.Floor_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Login = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Floors", "HSEBuilding_ID", "dbo.HSEBuildings");
            DropForeignKey("dbo.Rooms", "Floor_Id", "dbo.Floors");
            DropIndex("dbo.Rooms", new[] { "Floor_Id" });
            DropIndex("dbo.Floors", new[] { "HSEBuilding_ID" });
            DropTable("dbo.Users");
            DropTable("dbo.Rooms");
            DropTable("dbo.Floors");
            DropTable("dbo.HSEBuildings");
        }
    }
}
