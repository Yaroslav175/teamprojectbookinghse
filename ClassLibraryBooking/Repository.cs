﻿using ClassLibraryBooking.Interfaces;
using ClassLibraryBooking.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryBooking
{
    public class Repository : IRepository
    {
        public class GeneralData
        {
            public List<HSEBuilding> Buildings { get; set; }
            public List<Room> Rooms { get; set; }
            public List<User> Users { get; set; }
            public List<Floor> Floors { get; set; }
        }
        private GeneralData _generalData;
        public User _authorizedUser;

        public List<HSEBuilding> Buildings => _generalData.Buildings;
        public List<Floor> Floors => _generalData.Floors;
        public List<Room> Rooms => _generalData.Rooms;

        private const string DataFolder = "BookingHSE/ClassLibrary/Data";
        private const string FileName = @"c:\users\кирилл\source\repos\BookingHSE\ClassLibraryBooking\Data\MainData.json";

        public Repository()
        {
            try
            {
                using (var sr = new StreamReader(Path.Combine(DataFolder, FileName)))
                {
                    using (var jsonReader = new JsonTextReader(sr))
                    {
                        var serializer = new JsonSerializer();
                        _generalData = serializer.Deserialize<GeneralData>(jsonReader);
                    }
                }

                foreach (var floor in _generalData.Floors)
                    foreach (var rooms in _generalData.Rooms)
                    {
                        //st.Station = _generalData.Stations.First(s => s.Id == st.StationId);
                        var room = _generalData.Rooms.First(r => r.Floor == floor.Id);
                    }
            }
            catch
            {
                _generalData = new GeneralData
                {
                    Buildings = new List<HSEBuilding>(),
                    Rooms = new List<Room>(),
                    Users = new List<User>()

                };
            }
        }

        public void RegisterUser(User user)
        {
            user.Id = _generalData.Users.Count > 0 ? _generalData.Users.Max(u => u.Id) + 1 : 1;
            _generalData.Users.Add(user);
            Save();
        }
        public bool Authorize(string login, string password)
        {
            var user = _generalData.Users.FirstOrDefault(u => u.Login == login && u.Password == Password.GetHash(password));
            if (user != null)
            {
                _authorizedUser = user;
                return true;
            }
            return false;
        }
        public void Save()
        {
            if (!Directory.Exists(DataFolder))
            {
                Directory.CreateDirectory(DataFolder);
            }
            using (var sw = new StreamWriter(Path.Combine(DataFolder, FileName)))
            {
                using (var jsonWriter = new JsonTextWriter(sw))
                {
                    var serializer = new JsonSerializer();
                    serializer.Serialize(jsonWriter, _generalData);
                }
            }
        }
        public static GeneralData DeserializeGeneralData(bool isCalledFromSeed)
        {
            string path;
            if (isCalledFromSeed)
            {
                path = "ClassLibraryBooking/Data/MainData.json";
            }
            else
            {
                path = "../../../ClassLibraryBooking/Data/MainData.json";
            }
            using (var sr = new StreamReader(path))
            {
                using (var jsonReader = new JsonTextReader(sr))
                {
                    var deserializer = new JsonSerializer();
                    return deserializer.Deserialize<GeneralData>(jsonReader);
                }
            }
        }
    }
}
