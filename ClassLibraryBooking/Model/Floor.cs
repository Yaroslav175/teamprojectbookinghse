﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryBooking.Model
{
    public class Floor
    {
        public int Id { get; set; }
        public List<Room> Rooms { get; set; }
        public string Number { get; set; }
    }
}
