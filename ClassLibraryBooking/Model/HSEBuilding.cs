﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibraryBooking.Model
{
    public class HSEBuilding
    {
        public int ID { get; set; }
        public string Address { get; set; }
        public List<Floor> Floors { get; set; }
    }
}
