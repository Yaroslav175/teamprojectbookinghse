﻿using ClassLibraryBooking;
using ClassLibraryBooking.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace BookingHSE
{
    /// <summary>
    /// Логика взаимодействия для DataSelectWindow.xaml
    /// </summary>
    public partial class DataSelectWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();
        public DataSelectWindow()
        {
            InitializeComponent();
        }

        private void ComboboxAddress_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void ComboboxDay_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void Results_Click(object sender, RoutedEventArgs e)
        {

        }
    }
}
