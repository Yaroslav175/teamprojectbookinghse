﻿using ClassLibraryBooking;
using ClassLibraryBooking.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace BookingHSE
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        IRepository _repo = Factory.Instance.GetRepository();
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_LoginClick(object sender, RoutedEventArgs e)
        {
            if (_repo.Authorize(textBoxLogin.Text, passwordBox.Password))
            {
                var dataSelectWindow = new DataSelectWindow();
                dataSelectWindow.Show();
                Close();
            }
            else
            {
                MessageBox.Show("Incorrect login/password");
            }
        }

        private void Button_RegisterClick(object sender, RoutedEventArgs e)
        {
            var registerWindow = new RegisterWindow();
            registerWindow.RegistrationFinished += RegisterWindow_RegistrationFinished;
            registerWindow.Show();

            Hide();
        }

        private void RegisterWindow_RegistrationFinished()
        {
            Show();
        }
    }
}
